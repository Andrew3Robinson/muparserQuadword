/* mathe.h header, version 0.5
 * Copyright (C) Nov 28,2017 and later by Andrew Robinson of Scappose,
 * but with deference to the Free Software Foundation's prior rights to portions
 * of this code.  This copyright notice can not be separated from this file
 * or modified without permission.
 *
 * Because this header can translate the quadword library, some of 
 * the code is legally a derivative of GNU public license version 2.0 software.
 * Therefore this code itself is subject to GPL copyright clauses
 * for derivative works. Software that uses this header, is not
 * necessarily a derivative work.
 *
 * All macro and function names in this translator which are 
 * also found in standard C libraries, or IEEE documents, are not to be 
 * encumbered by a GPL copyright. Users who only invoke standard
 * C,C++ or IEEE names, or those names extended with prefixes  or suffixes
 * not found in the GCC Quad-Prection Math Libarary are not making a derivative
 * work.
 * Any standard names having an 'E','e' suffix or USE prefix are also
 * not part of the Quad-Precision Math Library.
 * A few examples are:  cos(), math.h, mathe.h, cmathe, floatE, cosE() 
 * NANe, USE_IEEE754R128, etc. None of these patterns of names were found in the 
 * Quad-Precision Math Library and are free for use in any-one's code. 
 *
 * However:
 * I do not grant the right to copy or use this software as a whole to certain
 * people (limited in number) who have been antagonistic towards quesions
 * about muParser on public sites, and discouraging cooperative improvement.
 * Please see the top level of the muparserQuadword repository for details.
 *
 * All other people have the right to copy this file verbatim, use it in their
 * projects, and modify the portions of code specific to the quadword library
 * in agreement with GPL license for derivative works.
 *
 * The original GPL license for quadword library follows:

   GCC Quad-Precision Math Library                                              
   Copyright (C) 2010, 2011 Free Software Foundation, Inc.                      
   Written by Francois-Xavier Coudert  <fxcoudert@gcc.gnu.org>   

   Libquadmath is free software; you can redistribute it and/or                    
   modify it under the terms of the GNU Library General Public                     
   License as published by the Free Software Foundation; either                    
   version 2 of the License, or (at your option) any later version.                
                                                                                
   Libquadmath (and muParser is distributed in the hope that it will be useful,                  
   but WITHOUT ANY WARRANTY; without even the implied warranty of                  
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU               
   Library General Public License for more details.                                
                                                                                
   You should have received a copy of the GNU Library General Public               
   License along with libquadmath; see the file COPYING.LIB.  If                   
   not, write to the Free Software Foundation, Inc., 51 Franklin Street - Fifth Floor,
   Boston, MA 02110-1301, USA.
*/                                          



#ifndef _mathe_h
#define _mathe_h

/* This is a macro wrapper math library
 * It's intended to help porting legacy code to multiple sized math
 * libraries.
 * 
 * The library is designed so that if a user codes with an 'E' suffix
 * on the end of the name of math function for doubles, the function
 * calls can be redirected to different math libraries based on a single
 * single _M_eT macro, which determines the actual suffix E is replaced
 * with. The same will apply to constants, eg: M_PIe, but the suffix appended
 * is lowercase to prevent any namespace conflicts.
 *
 * #define _M_eT f // direct E suffixed function calls to "float"
 * #define _M_eT   // direct E suffixed function calls to "double"
 * #define _M_eT q // direct E suffixed function calls to "__float128"
 * #define _M_eT l // direct E suffixed function calls to "long double"
 *
 * in user code,
 * cosE(1.0) // will be call whichever math library size _M_eT is.
 * _M_PIe    // will be set to whichever constant size _M_eT is.
 *
 * Additionally, if a program is already coded for doubles,
 * macro overloading can be attempted to replace all math.h function calls
 * for doubles, with a different size function.  This is not desirable,
 * for production; but it allows a quick test of code portability.
 *
 * To attempt a macro overload,
 * #define double floatE_t
 * The header will detect the double overload, and attempt to redefine
 * all double functions with ones of the size floatE_t, determined
 * by _M_eT.
*/ 

#define _M_eee(f,p) f ## p
#define _M_ee(f,p) _M_eee(f,p)

/* All possible math functions. */
#define snprintfE	_M_ee( snprintf,_M_eT )
#define strtodE		_M_ee( strtod,	_M_eT )
#define acosE		_M_ee( acos,	_M_eT )
#define acoshE		_M_ee( acosh,	_M_eT )
#define asinE		_M_ee( asin,	_M_eT )
#define asinhE		_M_ee( asinh,	_M_eT )
#define atanE		_M_ee( atan,	_M_eT )
#define atanhE		_M_ee( atanh,	_M_eT )
#define atan2E		_M_ee( atan,	_M_eT )
#define cbrtE		_M_ee( cbrt,	_M_eT )
#define ceilE		_M_ee( ceil,	_M_eT )
#define copysignE	_M_ee( copysign,_M_eT )
#define coshE		_M_ee( cosh,	_M_eT )
#define cosE		_M_ee( cos,	_M_eT )
#define erfE		_M_ee( erf,	_M_eT )
#define erfcE		_M_ee( erfc,	_M_eT )
#define expE		_M_ee( exp,	_M_eT )
#define expm1E		_M_ee( expm1,	_M_eT )
#define fabsE		_M_ee( fabs,	_M_eT )
#define fdimE		_M_ee( fdim,	_M_eT )
#define finiteE		_M_ee( finite,	_M_eT )
#define floorE		_M_ee( floor,	_M_eT )
#define fmaE		_M_ee( fma,	_M_eT )
#define fmaxE		_M_ee( fmax,	_M_eT )
#define fminE		_M_ee( fmin,	_M_eT )
#define fmodE		_M_ee( fmod,	_M_eT )
#define frexpE		_M_ee( frexp,	_M_eT )
#define hypotE		_M_ee( hypot,	_M_eT )
#define ilogbE		_M_ee( ilogb,	_M_eT )
#define isinfE		_M_ee( isinf,	_M_eT )
#define isnanE		_M_ee( isnan,	_M_eT )
#define j0E		_M_ee( j0,	_M_eT )
#define j1E		_M_ee( j1,	_M_eT )
#define jnE		_M_ee( jn,	_M_eT )
#define ldexpE		_M_ee( ldexp,	_M_eT )
#define lgammaE		_M_ee( lgamma,	_M_eT )
#define llrintE		_M_ee( llrint,	_M_eT )
#define llroundE	_M_ee( llround, _M_eT )
#define logbE		_M_ee( logb,	_M_eT )
#define logE		_M_ee( log,	_M_eT )
#define log10E		_M_ee( log10,	_M_eT )
#define log1pE		_M_ee( log1p,	_M_eT )
#define log2E		_M_ee( log2,	_M_eT )
#define lrintE		_M_ee( lrint,	_M_eT )
#define lroundE		_M_ee( lround,	_M_eT )
#define modfE		_M_ee( modf,	_M_eT )
#define nanE		_M_ee( nan,	_M_eT )
#define nearbyintE	_M_ee( nearybyint, _M_eT )
#define nextafterE	_M_ee( nextafter, _M_eT )
#define powE		_M_ee( pow,	_M_eT )
#define remainderE	_M_ee( remainder, _M_eT )
#define remquoE		_M_ee( remquo,	_M_eT )
#define rintE		_M_ee( rint,	_M_eT )
#define roundE		_M_ee( round,	_M_eT )
#define scalblnE	_M_ee( scalbln,	_M_eT )
#define scalbnE		_M_ee( scalbn,	_M_eT )
#define signbitE	_M_ee( signbit, _M_eT )
#define sincosE		_M_ee( sincos,	_M_eT )
#define sinhE		_M_ee( sinh,	_M_eT )
#define sinE		_M_ee( sin,	_M_eT )
#define sqrtE		_M_ee( sqrt,	_M_eT )
#define tanE		_M_ee( tan,	_M_eT )
#define tanhE		_M_ee( tanh,	_M_eT )
#define tgammaE		_M_ee( tgamma,	_M_eT )
#define truncE		_M_ee( trunc,	_M_eT )
#define y0E		_M_ee( y0,	_M_eT )
#define y1E		_M_ee( y1,	_M_eT )
#define ynE		_M_ee( yn,	_M_eT )

/* complex functions, should be broken out into headers
   complexe.h  ccomplexe
   however, they are bundled here for simplicity for now.
*/

#define cabsE		_M_ee( cabs,	_M_eT )
#define cargE		_M_ee( carg,	_M_eT )
#define cimagE		_M_ee( cimag,	_M_eT )
#define crealE		_M_ee( creal,	_M_eT )
#define cacoshE		_M_ee( acosh,	_M_eT )
#define cacosE		_M_ee( cacos,	_M_eT )
#define casinhE		_M_ee( casin,	_M_eT )
#define catanhE		_M_ee( catanh,	_M_eT )
#define catanE		_M_ee( catan,	_M_eT )
#define ccosE		_M_ee( ccos,	_M_eT )
#define ccoshE		_M_ee( ccosh,	_M_eT )
#define cexpE		_M_ee( cexp,	_M_eT )
#define cexpiE		_M_ee( cexpi,	_M_eT )
#define clogE		_M_ee( clog,	_M_eT )
#define clog10E		_M_ee( clog10,	_M_eT )
#define conjE		_M_ee( conj,	_M_eT )
#define cpowE		_M_ee( cpow,	_M_eT )
#define cprojE		_M_ee( cproj,	_M_eT )
#define csinE		_M_ee( csin,	_M_eT )
#define csinhE		_M_ee( csinh,	_M_eT )
#define csqrtE		_M_ee( csqrt,	_M_eT )
#define ctanE		_M_ee( ctan,	_M_eT )
#define ctanhE		_M_ee( ctanh,	_M_eT )


// Define the appropriate data type symbols for extended type.
#define _M_TYPEf  1
#define _M_TYPEd  2
#define _M_TYPEld 3
#define _M_TYPEq  4
#define _M_TYPEe _M_ee( _M_TYPE,	_M_eT )

#if defined( USE_IEEE754R128 ) || _M_TYPEe == _M_TYPEq
#	include <quadmath.h> // float limits are already part of this header.
#endif

#include <math.h>
#include <float.h>

#if _M_TYPEe == _M_TYPEd
#	define  M_eT ""    // no extension type; double is default
#	undef  _M_eT
#	define _M_eT
#	define M_eCSUFFIX  // no constant extension qualifier, as double is default.
#	ifdef double
#		undef double
#	endif
#	ifndef	floatE_t
#		define  floatE_t  floatE_t
		typedef double floatE_t;
#	endif
#	define FLTe_MAX_DIGITS  	19
#	ifdef DBL_MAX
#	define	FLTe_MAX		DBL_MAX
#	endif
#	ifdef DBL_MIN
#	define	FLTe_MIN		DBL_MIN	
#	endif
#	ifdef DBL_EPSILON
#	define	FLTe_EPSILON		DBL_EPSILON
#	endif
#	ifdef DBL_DENORM_MIN
#	define	FLTe_DENORM_MIN		DBL_DENORM_MIN
#	endif
#	ifdef DBL_MANT_DIG
#	define	FLTe_MANT_DIG		DBL_MANT_DIG
#	endif
#	ifdef DBL_MIN_EXP
#	define  FLTe_MIN_EXP		DBL_MIN_EXP
#	endif
#	ifdef DBL_MAX_EXP
#	define	FLTe_MAX_EXP		DBL_MAX_EXP
#	endif
#	ifdef DBL_DIG
#	define  FLTe_DIG		DBL_DIG
#	endif
#	ifdef DBL_MIN_10_EXP
#	define	FLTe_MIN_10_EXP		DBL_MIN_10_EXP
#	endif
#	ifdef DBL_MAX_10_EXP
#	define	FLTe_MAX_10_EXP 	DBL_MAX_10_EXP
#	endif
#elif _M_TYPEe == _M_TYPEf
#	define M_eT "" // no extension type; floats promote to double
#	define M_eCSUFFIX  F  // float constant suffix
#	undef  _M_eT
#	define _M_eT
#	ifndef	floatE_t
#		define  floatE_t	floatE_t
		typedef float floatE_t;
#	endif
#	ifdef FLT_MAX
#	define	FLTe_MAX		FLT_MAX
#	endif
#	ifdef FLT_MIN
#	define	FLTe_MIN		FLT_MIN	
#	endif
#	ifdef FLT_EPSILON
#	define	FLTe_EPSILON		FLT_EPSILON
#	endif
#	ifdef FLT_DENORM_MIN
#	define	FLTe_DENORM_MIN	FLT_DENORM_MIN
#	endif
#	ifdef FLT_MANT_DIG
#	define	FLTe_MANT_DIG		FLT_MANT_DIG
#	endif
#	ifdef FLT_MIN_EXP
#	define  FLTe_MIN_EXP		FLT_MIN_EXP
#	endif
#	ifdef FLT_MAX_EXP
#	define	FLTe_MAX_EXP		FLT_MAX_EXP
#	endif
#	ifdef FLT_DIG
#	define  FLTe_DIG		FLT_DIG
#	endif
#	ifdef FLT_MIN_10_EXP
#	define	FLTe_MIN_10_EXP		FLT_MIN_10_EXP
#	endif
#	ifdef FLT_MAX_10_EXP
#	define	FLTe_MAX_10_EXP 	FLT_MAX_10_EXP
#	endif
#elif _M_TYPEe==_M_TYPEl
#	define M_eT "l"			// long float extension type
#	define M_eCSUFFIX L		// long float const qualifier suffix
#	ifndef	floatE_t
#		define  floatE_t	floatE_t
		typedef long double floatE_t;
#	endif
#	ifdef LDBL_MAX
#	define	FLTe_MAX		LDBL_MAX
#	endif
#	ifdef LDBL_MIN
#	define	FLTe_MIN		LDBL_MIN	
#	endif
#	ifdef LDBL_EPSILON
#	define	FLTe_EPSILON		LDBL_EPSILON
#	endif
#	ifdef LDBL_DENORM_MIN
#	define	FLTe_DENORM_MIN		LDBL_DENORM_MIN
#	endif
#	ifdef LDBL_MANT_DIG
#	define	FLTe_MANT_DIG		LDBL_MANT_DIG
#	endif
#	ifdef LDBL_MIN_EXP
#	define  FLTe_MIN_EXP		LDBL_MIN_EXP
#	endif
#	ifdef LDBL_MAX_EXP
#	define	FLTe_MAX_EXP		LDBL_MAX_EXP
#	endif
#	ifdef LDBL_DIG
#	define  FLTe_DIG		LDBL_DIG
#	endif
#	ifdef LDBL_MIN_10_EXP
#	define	FLTe_MIN_10_EXP		LDBL_MIN_10_EXP
#	endif
#	ifdef LDBL_MAX_10_EXP
#	define	FLTe_MAX_10_EXP 	LDBL_MAX_10_EXP
#	endif
#elif _M_TYPEe == _M_TYPEq
#	define  M_eT "Q"		// quad float extension type
#	define  M_eCSUFFIX Q		// quad float const qualifier suffix
#	ifndef	floatE_t
#		define  floatE_t	floatE_t
		typedef __float128 floatE_t;
#	endif
#	define strtodq	strtoflt128
#	define snprintfq quadmath_snprintf
#	ifdef FLT128_MAX
#	define	FLTe_MAX		FLT128_MAX
#	endif
#	ifdef FLT128_MIN
#	define	FLTe_MIN		FLT128_MIN
#	endif
#	ifdef FLT128_EPSILON
#	define	FLTe_EPSILON		FLT128_EPSILON
#	endif
#	ifdef FLT128_DENORM_MIN
#	define	FLTe_DENORM_MIN		FLT128_DENROM_MIN
#	endif
#	ifdef FLT128_MANT_DIG
#	define	FLTe_MANT_DIG		FLT128_MANT_DIG
#	endif
#	ifdef	FLT128_MIN_EXP
#	define  FLTe_MIN_EXP		FLT128_MIN_EXP
#	endif
#	ifdef	FLT128_MAX_EXP
#	define	FLTe_MAX_EXP		FLT128_MAX_EXP
#	endif
#	ifdef	FLT128_DIG
#	define  FLTe_DIG		FLT128_DIG
#	endif
#	ifdef	FLT128_MIN_10_EXP
#	define	FLTe_MIN_10_EXP		FLT128_MIN_10_EXP
#	endif
#	ifdef	FLT128_MAX_10_EXP
#	define	FLTe_MAX_10_EXP 	FLT128_MAX_10_EXP
#	endif
#else
#error "Unknown _M_TYPEe macro expansion of _M_eT for extended float definition."
#endif

#undef _M_TYPEf
#undef _M_TYPE
#undef _M_TYPEl
#undef _M_TYPEq
#undef _M_TYPEe

// Generic values derivable from other values already given.
#define _M_eeestr(x) #x
#define _M_eestr(x) _M_eeestr(x)
#define _M_estr(x) _M_eestr(x)
#define  FLTe_FORMATGMAX  "%." _M_estr( FLTe_DIG ) M_eT "g"

// -----------------------------------------------------------------------
#ifdef double  // We are attempting a dangerous quick-port of a double app.
#ifdef NAN
#undef NAN
#endif
#define NAN	(__NANe())
#ifdef HUGE_VAL
#undef HUGE_VAL
#endif
#define HUGE_VAL (__HUGE_VALe())

#ifdef	M_E
#	undef  M_E
#endif
#define M_E M_Ee
#ifdef  M_LOG2E
#	undef  M_LOG2E
#endif
#define M_LOG2E	M_LOG2Ee
#ifdef M_LOG10E
#	undef  M_LOG10E
#endif
#define M_LOG10E M_LOG10Ee
#ifdef M_LN2
#	undef  M_LN2
#endif
#define M_LN2 M_LN2e
#ifdef M_LN10
#	undef  M_LN10
#endif
#define M_LN10	M_LN10e
#ifdef M_PI
#	undef  M_PI
#endif
#define M_PI M_PIe
#ifdef M_PI_2
#	undef  M_PI_2
#endif
#define M_PI_2	M_PI_2e
#ifdef M_PI_4
#	undef  M_PI_4
#endif
#define M_PI_4	M_PI_4e
#ifdef M_1_PI
#	undef  M_1_PI
#endif
#define M_1_PI	M_1_PIe
#ifdef M_2_PI
#	undef  M_2_PI
#endif
#define M_2_PI	M_2_PIe
#ifdef M_@_SQRTPI
#	undef  M_2_SQRTPI
#endif
#define M_2_SQRTPI M_2_SQRTPIe
#ifdef M_SQRT2
#	undef  M_SQRT2
#endif
#define M_SQRT2	M_SQRT2e
#ifdef M_SQRT1_2
#	undef  M_SQRT1_2
#endif
#define M_SQRT1_2 M_SQRT1_2

/* All possible double math functions are macro defined as another size 
 * This code is difficult to use, properly.
 */

#define snprintf	_M_ee( snprintf,_M_eT )
#define strtod		_M_ee( strtod,	_M_eT )
#define acos		_M_ee( acos,	_M_eT )
#define acosh		_M_ee( acosh,	_M_eT )
#define asin		_M_ee( asin,	_M_eT )
#define asinh		_M_ee( asinh,	_M_eT )
#define atan		_M_ee( atan,	_M_eT )
#define atanh		_M_ee( atanh,	_M_eT )
#define atan2		_M_ee( atan,	_M_eT )
#define cbrt		_M_ee( cbrt,	_M_eT )
#define ceil		_M_ee( ceil,	_M_eT )
#define copysign	_M_ee( copysign,_M_eT )
#define cosh		_M_ee( cosh,	_M_eT )
#define cos			_M_ee( cos,		_M_eT )
#define erf			_M_ee( erf,		_M_eT )
#define erfc		_M_ee( erfc,	_M_eT )
#define exp			_M_ee( exp,		_M_eT )
#define expm1		_M_ee( expm1,	_M_eT )
#define fabs		_M_ee( fabs,	_M_eT )
#define fdim		_M_ee( fdim,	_M_eT )
#define finite		_M_ee( finite,	_M_eT )
#define floor		_M_ee( floor,	_M_eT )
#define fma			_M_ee( fma,		_M_eT )
#define fmax		_M_ee( fmax,	_M_eT )
#define fmin		_M_ee( fmin,	_M_eT )
#define fmod		_M_ee( fmod,	_M_eT )
#define frexp		_M_ee( frexp,	_M_eT )
#define hypot		_M_ee( hypot,	_M_eT )
#define ilogb		_M_ee( ilogb,	_M_eT )
#define isinf		_M_ee( isinf,	_M_eT )
#define isnan		_M_ee( isnan,	_M_eT )
#define j0			_M_ee( j0,		_M_eT )
#define j1			_M_ee( j1,		_M_eT )
#define jn			_M_ee( jn,		_M_eT )
#define ldexp		_M_ee( ldexp,	_M_eT )
#define lgamma		_M_ee( lgamma,	_M_eT )
#define llrint		_M_ee( llrint,	_M_eT )
#define llround		_M_ee( llround, _M_eT )
#define logb		_M_ee( logb,	_M_eT )
#define log			_M_ee( log,		_M_eT )
#define log10		_M_ee( log10,	_M_eT )
#define log1p		_M_ee( log1p,	_M_eT )
#define log2		_M_ee( log2,	_M_eT )
#define lrint		_M_ee( lrint,	_M_eT )
#define lround		_M_ee( lround,	_M_eT )
#define modf		_M_ee( modf,	_M_eT )
#define nan			_M_ee( nan,		_M_eT )
#define nearbyint	_M_ee( nearybyint, _M_eT )
#define nextafter	_M_ee( nextafter, _M_eT )
#define pow			_M_ee( pow,		_M_eT )
#define remainder	_M_ee( remainder, _M_eT )
#define remquo		_M_ee( remquo,	_M_eT )
#define rint		_M_ee( rint,	_M_eT )
#define round		_M_ee( round,	_M_eT )
#define scalbln		_M_ee( scalbln,	_M_eT )
#define scalbn		_M_ee( scalbn,	_M_eT )
#define signbit		_M_ee( signbit, _M_eT )
#define sincos		_M_ee( sincos,	_M_eT )
#define sin			_M_ee( sinh,	_M_eT )
#define sin			_M_ee( sin,		_M_eT )
#define sqrt		_M_ee( sqrt,	_M_eT )
#define tan			_M_ee( tan,		_M_eT )
#define tanh		_M_ee( tanh,	_M_eT )
#define tgamma		_M_ee( tgamma,	_M_eT )
#define trunc		_M_ee( trunc,	_M_eT )
#define y0			_M_ee( y0,		_M_eT )
#define y1			_M_ee( y1,		_M_eT )
#define yn			_M_ee( yn,		_M_eT )

/* complex functions */

#define cabs		_M_ee( cabs,	_M_eT )
#define carg		_M_ee( carg,	_M_eT )
#define cimag		_M_ee( cimag,	_M_eT )
#define creal		_M_ee( creal,	_M_eT )
#define cacosh		_M_ee( acosh,	_M_eT )
#define cacos		_M_ee( cacos,	_M_eT )
#define casinh		_M_ee( casin,	_M_eT )
#define catanh		_M_ee( catanh,	_M_eT )
#define catan		_M_ee( catan,	_M_eT )
#define ccos		_M_ee( ccos,	_M_eT )
#define ccosh		_M_ee( ccosh,	_M_eT )
#define cexp		_M_ee( cexp,	_M_eT )
#define cexpi		_M_ee( cexpi,	_M_eT )
#define clog		_M_ee( clog,	_M_eT )
#define clog10		_M_ee( clog10,	_M_eT )
#define conj		_M_ee( conj,	_M_eT )
#define cpow		_M_ee( cpow,	_M_eT )
#define cproj		_M_ee( cproj,	_M_eT )
#define csin		_M_ee( csin,	_M_eT )
#define csinh		_M_ee( csinh,	_M_eT )
#define csqrt		_M_ee( csqrt,	_M_eT )
#define ctan		_M_ee( ctan,	_M_eT )
#define ctanh		_M_ee( ctanh,	_M_eT )

#endif

// Define gnu constants with enough digits to convert to the extension type without loss, up to quad word.
#ifdef __USE_GNU
# define M_Ee		( _M_ee( 2.718281828459045235360287471352662498 , _M_eCSUFFIX ) ) /* e */
# define M_LOG2Ee	( _M_ee( 1.442695040888963407359924681001892137 , _M_eCSUFFIX ) ) /* log_2 e */
# define M_LOG10Ee	( _M_ee( 0.434294481903251827651128918916605082 , _M_eCSUFFIX ) ) /* log_10 e */
# define M_LN2e		( _M_ee( 0.693147180559945309417232121458176568 , _M_eCSUFFIX ) ) /* log_e 2 */
# define M_LN10e	( _M_ee( 2.302585092994045684017991454684364208 , _M_eCSUFFIX ) ) /* log_e 10 */
# define M_PIe		( _M_ee( 3.141592653589793238462643383279502884 , _M_eCSUFFIX ) ) /* pi */
# define M_PI_2e	( _M_ee( 1.570796326794896619231321691639751442 , _M_eCSUFFIX ) ) /* pi/2 */
# define M_PI_4e	( _M_ee( 0.785398163397448309615660845819875721 , _M_eCSUFFIX ) ) /* pi/4 */
# define M_1_PIe	( _M_ee( 0.318309886183790671537767526745028724 , _M_eCSUFFIX ) ) /* 1/pi */
# define M_2_PIe	( _M_ee( 0.636619772367581343075535053490057448 , _M_eCSUFFIX ) ) /* 2/pi */
# define M_2_SQRTPIe	( _M_ee( 1.128379167095512573896158903121545172 , _M_eCSUFFIX ) ) /* 2/sqrt(pi) */
# define M_SQRT2e	( _M_ee( 1.414213562373095048801688724209698079 , _M_eCSUFFIX ) ) /* sqrt(2) */
# define M_SQRT1_2e	( _M_ee( 0.707106781186547524400844362104849039 , _M_eCSUFFIX ) ) /* 1/sqrt(2) */
#endif

static inline const floatE_t _NANe(void) {
	return (-(const floatE_t)(*(const float*)(const void*)(const unsigned long[4]){ -1UL,-1UL,-1UL,-1UL }));
}
#define NANe		(_NANe())

static inline const floatE_t __HUGE_VALe(void) {
	return ((const floatE_t)(FLTe_MAX+FLTe_MAX) ) ;
}
#define HUGE_VALe 	(_HUGE_VALe())



#endif // once header block
