# muparserQuadword

This is an experimental fork of the muparser library which uses the gnu quadword
library to implement a 128 bit floating point type, and arbitrary user functions.

The original idea for this fork was deleted by Shog9♦ Sep 21 at 16:56
https://stackoverflow.com/questions/51888728/mu-parser-dynamically-adding-arbitrary-user-functio

Shog9, didn't read the idea carefully and deleted it based on false accusations.
He was obviously covering up other moderators failures on StackOverflow.

This is what my thread looked like before I removed the header code, and placed it in gitlab.
https://web.archive.org/web/20180831180821/https://stackoverflow.com/questions/51888728/mu-parser-dynamically-adding-arbitrary-user-functions-at-run-time-wanting-bett?noredirect=1%23comment91135700_51888728

If you have an S.O. account, you can review the deleted thread and clearly see
that the copyrighted information was removed before shog9 deleted
the thread in error.  Recovery of the thread is not possible without several
people complaining, and that is not likely give S.O.'s retentive culture.

Experimenting with the stack overflow question can be done by:
git clone --branch StackOverflowQuestion https://gitlab.com/Andrew3Robinson/muparserQuadword

You can then copy and paste the answers from the web.archive.org link above.

later commits to this repository will attempt to extend and improve muParser,
and will make user functions part of the library itself rather than example
code. StackOverflow copyright paranoia will not apply....

In this repository, the work I've done is meant to be applied to other projects.
The open source headers I wrote are designed to aid portable projects in changing
their floating point size when transferring from one system to another.
They headers do not require muparser to be used, and can be applied to other
floating point projects.

The purpose of my headers (cmath,cmath.h) is to simplify projects (and muparser)
that use floating point as taught in professional software engineering
courses on portability.  If your project dosn't follow floating point portability
guidelines, then the headers will be less useful to you. :(

Unfortunately, Many packages can not be upgraded easily to use GCC quad-math floating point.
Upgrading is especially difficult in any package using optimizations based on IEEE 
floating point packing of structures, or pagakes depending on "double" being the
largest portable size avaiable in C/C++ programs.
( Gnuplot and Octave are an excellent examples. )

These issues have become legacy in many other unix and windows programs as well.
Truly portable code is supposed to rely on limits.h, climits, etc. and not
a specific float type if possible.

Unfortunately, GCC's supplied floating point headers don't implement size agnostic
compatability wrappers for GCC quadmath (as of 2018).  By size agnostic I mean,
headers that allow a simple change from compiling with double to compiling with
quadword by  just changing a type-definition, or (by using C macros, sed, or awk).

If  your code is written portably, upgrades ought to be as
simple as changing a typedef in a header someplace ... and #including 
my math headers as a convenience header for your project in place of math.h
cmath limits.h , etc. 

That's why I wrote my headers cmath.h, cmath.  I want to share convenience headers
that help manage floating point porting to all standard C/C++ float sizes
including extended bit sizes as they become available. (eg: IEEE 754 R 128 ).

muParser is an example package that can now be compile dfor any floating point size,
by just changing a #define.  

Note:
The mathe.h and cmathe headers are released as gnu public license 2.0.  (GPL-2.0)
I am required to release them under GPL, because in all honesty, they are derived from 
the quadmath headers of the GNU project!  

However the namespace that I'm exporting is not GPL2.0 code, so you may use
my headers in code that is not itself a QuadMath derivative or GPL code.
If you need some of the work in my headers released under a different license;
talk to me about it, and I'll look into it.

The code necessary to implement limits.h, limits, and classes associated with
floating point size agnostic code; are bundled inside the two headers I've made.
They should be broken out, but that's above my skill level at this time.

Some of the limits given in my header files may not be entirely correct,
because Quadmath documents were not complete at the time I wrote my headers.
There are some vague points in the floating point standards which I don't
entirely understand.

However, I've found the values which are missing or (slightly) wrong, probably
don't apply to most projects.  If you find a mistake, let me know; and I will
veify what the value ought to be and upgrade my open source headers.

My repository obviously has new and edited files compared to the original muParser.
Original repository:  https://github.com/beltoforion/muparser

Patch files to upgrade from the original muparser git repository to this one
will be marked with .gentoo.patch on the end.  The diff files are not needed to 
compile the code in this repository.  They are for reference, and to use
on Gentoo systems as a way to upgrade muparser without writing  a new ebuild
script.

Installing my patches in /etc/portage/patch/muparser..../ should auto upgrade 
muparser on Gentoo.  ( Patches will be available by end of October 2018).

I was very pleasantly surprised at how simple porting muparser to quadwords was.
It restored my faith, that some college courses on portability are listened
to by students.

Good luck.
--Andrew Robinson of Scappoose.

--------------------------------------------------------------------------------
META META META META

*Is stackoverflow increasing the power of stupid(?) or unethical people and 
encouragigning avoidable concflicts by not having balances on it's voting system?*

--------------------------------------------------------------------------------

Consider my thread:

https://web.archive.org/web/20180831180821/https://stackoverflow.com/questions/51888728/mu-parser-dynamically-adding-arbitrary-user-functions-at-run-time-wanting-bett?noredirect=1%23comment91135700_51888728

The negative -13 votes on Aug 31, was because of contradicotry complaints made
by people.  There is no requirement for a negative vote to have a consensus.
One negative vote can be because, "it's too short"; another "because it's too long."
The latest editor of my thread even removed a mandatory GPL derivative copy notice.
And they put just enough comments that I can't respond without my response not
being  shown;  so here it is:


"@halfer, Please chat before editing.
I don't interpret the agreement I made with SO in the same way you do,
nor does my attorney. Note: The GPL notice can not legally be removed by you.
I gave Stack overflow the right to copy my work, but I can't give them the right
to edit it arbitrarily. eg: because under U.S. copyright law, a wrapper of a novel
GPL library (not LGPL) is still required to obey fair use laws. Clearly, QuadMath
library is a novel original work. My wrapper of QUADMATH automatically requires
a GPL notice for novel calls. Code using my header does not require GPL notice.
– Andrew of Scappoose 5 mins ago"

The wrapper header was moved to the repository; and is not on stack overflow.
Anyone who reads the thread can see that the copyrighted code is removed.

I had over 181 reputation points before starting this project, and was reduced to
156 reputation points in a matter of hours because of muParser and a valid question.

That's over 10% of my reputation ... for what?  Being a good citizen, and
giving away free source code solutions to problems I've come across; while asking
"Does anyone know how to improve the solutions available for muParser."

Stack overflow is supposed to be a place for answers being rewarded.  
But, they only want to reward people with bronze badges and reputation points.
The adrenaline addicted reputation point people ... are what's killing the site.
Try to offer a bonus, for anyone who answer's your question ...
It's withing the site rules to say, "If I get an answer, I'll throw in another answer as a bonus."

That will get attacked as if it's heresy .. even though there's no rule against
it in the actual site regulations.  It's what the sites about ... better CODE!

Rather than supply answers, theres a whole slew of "thought police" (or as some
extremests on the web call them, soup NAZI's)

The amazing thing is how little room for dialog there is on the site.
It's a site full of people who are suspicous, downvote based on suspicion and
not proof, then only ask questions later after trying to blame the other person
kind of site.

I'm not alone in being bothered by StackOverflow for the mob mentality
it fosters; and the frustration it causes creative people who just want to discuss
and explore answerable questions.

Sometimes, questions don't follow site rules ... and deserve a downvote.
BUt OFTEN there's only anti-open software bigotry, suspicion, and envy based
on selfishness.   And that's the main reason for downvotes, today.

I believe the insane downvote issue is mostly about ignorant people downvoting others.
Why would they do this? Because they know they can't get any reputation points
off of a question that requires creativity to answer. eg: It's too hard for them.
They've spent a lot of time scanning through questions looking for something easy
to answer.  My muparser thread frustrated them.

The main offenders are new programmers who have just finished classes, and
are full of theoretical knowledge, but little experience in making tough 
choices.


Therefore, they downvote (or edit) in hopes of cauing enough pain and ruin, that
the question gets dumbed-down for them, specifically.  Besides, if they can
make a slam put-down comment that other people like, their comment will
get upvoted. Even if you prove these people wrong, they still have
earned reptuation points by slamming you AND RUINING YOUR REPUTATION AT THE SAME
TIME!!!.  Their post may get deleted, but their downvote will still REMAIN.
So hit and run slamming becomes is a lifestyle on Stack Overflow, because:

The upvotes received trashing people are a reward.  There's no real
DISINCENTIVE to stop trashing other people.  Even if trash voters are good
99% of the time, but downvote 1% of the time.  There's still over a million
people on stackOverflow.  Downvotes pour in by the minute.  Upvotes, DON'T.

The most common accusation is: "You're trying to get me to do your work for you.",
That complaint will cause a loss of a ton of reputation fast, even though it's
an unfounded suspicion in many cases.
( For me, I earn nothing ... so the accusation is pathetic. )

I did an experiment, to show how a question that *might* be homework 
get's treated. I decided to answer  the question ... even though the question
is under suspicion.

I gave a creative solution that is perfectly legal C code.
I however, purposely set the answer up so that only if it is used by
a person cheating on homework ... the teacher will notice the coding style.

My answer got down-voted in seconds for reasons that had nothing to do with
forum rules.  Rather the unpopularity was purely that I didn't want to harass
the person, but give them an answer they couldn't get full credit for on 
homework; but is still an answer that still helps a person trying to learn.

Every programmer learning C, knows about formatting allowing multiple 
statements on one line.

https://web.archive.org/web/20180919153245/https://stackoverflow.com/questions/52398698/storing-text-lines-in-an-array-c/52399141

Also notice the hypocirsy of many of the qestions in the comments section.
People aren't supposed to answer questions in the comments section.
In fact, they are supposed to post answers that compete against my answer. 
When that is done, people get upvoted more ... but never downvoted.
Yet I got downvoted for making simple aesthetic choices that people didn't like.

People who post on stack overflow spend more time defending against
aesthetic issues and preferences than they do about the validity of an answer.
It took me less time to write an answer than it
took the people in that picture to post their complaints.

I've been programming for over 25 years.  I find being nitpicked by 
goodie-twoshoes programmers for catch-22 situations, *very* annoying.

In order to show I've thought through a problem ... I have to add explanations;
that results in  being "corrected" by people who knew my solution wasn't
defective in any real way in the first place; just in "aesthetic" ways.

This is why I put in my own question on muParser that I don't earn money or a
grade from the project.  I'm purely into it for learning and sharing reasons.
I didn't want to be bashed for "trying to get people to do my work for me."
But that just leads to being based for "starting a fight" ...
I'm mean, come ON!  Seriously, momst sites have tags for "beginner question"
vs. "Graduate school question" ; but not stack overflow.  All question have to
be dumbed down the lowest possible level or the "soup Nazi's" attack.


REALLY:

Stackoverflow needs to at least consider a policy, that if you downvote a question;
you aren't allowed to receive any positive reputation from it. EVER.
Automatic, if you downvote the question .. you should not be allowed to
enter the question until ALL your downvotes for that question are removed.
It's perfectly OK to talk to someone BEFORE downvoting them !!!

This is what my page and downvote looked like after a few hours of posting:
https://web.archive.org/web/20180818200914/https://stackoverflow.com/questions/51888728/mu-parser-dynamically-define-a-function

Notice, the conflict mentioned in the comments is because a person downvoted me
for claiming the project included C, with a C tag.  They removed the tag,
penalized me, made rude comments and were upvoted by another person.
But, the very person I refuted by posting a C answer to their false
accusation is still named on the closure notice.

My experience really does deserve to be added to the hall of shame for stack overflow.
Karma, number of upvotes, or bronze badges != Right or better programmer in many cases.
It just means the ignorant can lead the ignorant more often than not
because they've caught the popular mind of a high posting minority.

(75% of people signed up for SO, never post again after asking 1 question.)

I'm not alone; the insanity of SO is well documented all over the web:

https://www.embeddedrelated.com/showarticle/741.php

https://readmystuff.wordpress.com/2009/08/31/stackoverflow-hall-of-shame-why-i-hate-stackoverflow/

https://www.reddit.com/r/programming/comments/5610uf/the_decline_of_stack_overflow_has_been_greatly/

The bottom line: 
I've responded to everyone who gave a reason for their downvote.
I even edited my question to take their complaints into consideration (even if
they were WRONG).

But the net result is still the same. People complain a question is vague, and
their compaints are vague ... or they demand you follow rules, that don't exist.
The question gets longer as I respond to all the lawyerly whining, and add
nitpicking clarifications.

Then the downvotes multiply, because the question is too long.
But the nature of clarification ... is that it requires more text (not less).

Notice, the CLOSURE complaint says I need to add more information to clarify.

If the readers of the thread were more intelligent ... this would not be a problem.
But, they only have one or two goals ... the karma points and pride of put downs.
The only hope many people have of turning a question they can't answer into Karma
points ... it a downvote, or a slam comment that someone else will upvote.

Consider:
Why not make ALL the people who vote on a closure, be requred to post a 
comment that must NOT BE REMOVED, before their closure vote can take effect?
If their comment is removed, their downvote should automatically be removed.

There are general categories for why people downvote; so it should be easy
for stack overflow to "correlate" the attackers of a thread by taking a poll
when the downvote is made to see if contradictory reasons are being made for
a downvote.

If the comments do not agree, it's like 10 judges ... there's a big difference
between 5 saying it's too long, 5 saying it's too short; and a majority vote.
The problem is, the downvotes are anonymous and lazy ... but the people writing
the questions are spending hours of frustration trying to deal with ignorance.

As a final comment, not ONE of the people who made negative comments to me has
ever answered a question about muParser before on StackOverflow.  Most of them,
have clearly never worked with it.  Even though I asked them to decide whether
they were going to spend time learning muParser before entering the thread;
They chose to ignore the warning, turn the warning itself into a fight,
and then edit my thread proving my point.

McDonalds put a "Coffee is HOT" warning on their coffee.  I've heard lots of
people saying -- that's because there are stupid people in the world and the
warning should not be there.   I have a different perspective,  McDonalds got
sued because there are ignorant (but intelligent) people who have never tried
coffee before.  Hence, a lawsuit against McDonald's won money even though the
person suing was accused of being stupid.  (all the way to the BANK! I think
they are smarter than most people!)

But, at heart there is a difference between stupidity and ignorance.
I've been ignorant at times; but ignorance can be cured.
What I'm certain of is that stupidity is forever.  
McDonald's learned the lesson, when will StackOverflow ?

--Andrew Robinson (of Scappoose)
